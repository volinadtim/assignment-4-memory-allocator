#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>
#define BLOCK_MIN_CAPACITY 32
#define INITIAL_SIZE 4096
#define INITIAL_MIN_SIZE 1
#define BLOCK_SIZE_1 32
#define BLOCK_SIZE_2 2048
#define BLOCK_SIZE_3 512

struct test_structure_1
{
    int32_t a;
    int32_t b;
};

struct test_structure_2
{
    char a_1[50];
    char a_2[50];
    char a_3[50];
    char a_4[50];
    char a_5[50];
    char a_6[50];
    char a_7[50];
    char a_8[50];
    char a_9[50];
    char a_10[50];
};

static void
test_single_block()
{
    const void *heap = heap_init(INITIAL_MIN_SIZE);
    debug_heap(stderr, heap);
    const struct block_header *head = heap;

    assert(size_from_capacity(head->capacity).bytes == REGION_MIN_SIZE);
    assert(head->is_free);
    assert(head->next == NULL);

    void *allocated_memory = _malloc(BLOCK_SIZE_1);
    assert(allocated_memory != NULL && "Failed to allocate memory");
    assert(head->capacity.bytes == BLOCK_MIN_CAPACITY);
    assert(!head->is_free);
    printf("Allocated successfully\n");

    _free(allocated_memory);
    assert(head->is_free);
    heap_term();
    printf("free_single_block test passed\n");
}

static void test_three_blocks()
{
    const void *heap = heap_init(INITIAL_MIN_SIZE);
    debug_heap(stderr, heap);
    const struct block_header *head = heap;

    assert(size_from_capacity(head->capacity).bytes == REGION_MIN_SIZE);
    assert(head->is_free);
    assert(head->next == NULL);

    void *allocated_memory1 = _malloc(BLOCK_SIZE_1);
    assert(allocated_memory1 != NULL && "Failed to allocate memory");
    assert(head->capacity.bytes == BLOCK_MIN_CAPACITY);
    assert(!head->is_free);

    void *allocated_memory2 = _malloc(BLOCK_SIZE_2);
    assert(allocated_memory2 != NULL && "Failed to allocate memory");
    assert(head->next != NULL);
    assert(!head->next->is_free);

    void *allocated_memory3 = _malloc(BLOCK_SIZE_3);
    assert(allocated_memory3 != NULL && "Failed to allocate memory");
    assert(head->next->next != NULL);
    assert(!head->next->next->is_free);
    assert(head->next->next->next != NULL);

    _free(allocated_memory2);
    assert(head->next->is_free);
    assert(head->next->next->next != NULL);

    _free(allocated_memory1);
    assert(head->is_free);
    assert(head->next->next->next == NULL);
    assert(head->next->next != NULL);

    _free(allocated_memory3);
    assert(head->next->is_free);
    assert(head->next->next == NULL);

    heap_term();
    printf("three_blocks test passed\n");
}

static void test_structure_blocks()
{
    const void *heap = heap_init(INITIAL_MIN_SIZE);
    debug_heap(stderr, heap);
    const struct block_header *head = heap;

    assert(size_from_capacity(head->capacity).bytes == REGION_MIN_SIZE);
    assert(head->is_free);
    assert(head->next == NULL);

    struct test_structure_1 *allocated_memory1 = _malloc(sizeof(struct test_structure_1));
    allocated_memory1->a = 123;
    allocated_memory1->b = 456;
    assert(allocated_memory1 != NULL && "Failed to allocate memory");
    assert(!head->is_free);
    assert(allocated_memory1->a == 123);
    assert(allocated_memory1->b == 456);

    struct test_structure_2 *allocated_memory2 = _malloc(sizeof(struct test_structure_2));
    assert(allocated_memory2 != NULL && "Failed to allocate memory");
    assert(head->next != NULL);
    assert(!head->next->is_free);

    _free(allocated_memory2);
    assert(head->next->is_free);
    assert(head->next->next->next != NULL);

    _free(allocated_memory1);
    assert(head->is_free);
    assert(head->next->next->next == NULL);
    assert(head->next->next != NULL);

    heap_term();
    printf("structure_blocks test passed\n");
}

static void test_out_of_memory_expansion()
{
    const void *heap = heap_init(INITIAL_SIZE);
    assert(heap != NULL && "Failed to initialize heap");
    debug_heap(stderr, heap);

    void *allocated_memory1 = _malloc(BLOCK_SIZE_1);
    assert(allocated_memory1 != NULL && "Failed to allocate memory");

    void *allocated_memory2 = _malloc(INITIAL_SIZE - BLOCK_SIZE_1);
    assert(allocated_memory2 != NULL && "Failed to allocate memory");

    void *allocated_memory3 = _malloc(BLOCK_SIZE_1);
    assert(allocated_memory3 != NULL && "Failed to allocate memory");

    heap_term();
    printf("out_of_memory_expansion test passed\n");
}

#define abs_(x) ((x) < 0 ? -(x) : (x))

int main()
{
    test_single_block();
    test_three_blocks();
    test_out_of_memory_expansion();

    printf("All tests passed\n");

    return 0;
}
